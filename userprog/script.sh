#!/bin/bash

batch1="../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70\n../test/testloop 70"
batch2="../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70\n../test/testloop1 70"
batch3="../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70\n../test/testloop2 70"
batch4="../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70\n../test/testloop3 70"

for i in $(seq 1 10); do
	echo -e $i>batches/batch1_$i
	echo -e $batch1>>batches/batch1_$i

	echo -e $i>>batches/batch2_$i
	echo -e $batch2>>batches/batch2_$i

	echo -e $i>>batches/batch3_$i
	echo -e $batch1>>batches/batch3_$i

	echo -e $i>>batches/batch4_$i
	echo -e $batch1>>batches/batch4_$i
done;

for i in $(seq 1 10); do
	echo -e "batch1_$i\n">>output.txt
	./nachos -F batches/batch1_$i|sed -n -e '/Machine/,$p'>>output.txt
	echo -e "batch2_$i\n">>output.txt
	./nachos -F batches/batch2_$i|sed -n -e '/Machine/,$p' >>output.txt
	echo -e "batch3_$i\n">>output.txt
	./nachos -F batches/batch3_$i|sed -n -e '/Machine/,$p' >>output.txt
	echo -e "batch4_$i\n">>output.txt
	./nachos -F batches/batch4_$i|sed -n -e '/Machine/,$p' >>output.txt
done;
