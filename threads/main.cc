// main.cc 
//	Bootstrap code to initialize the operating system kernel.
//
//	Allows direct calls into internal operating system functions,
//	to simplify debugging and testing.  In practice, the
//	bootstrap code would just initialize data structures,
//	and start a user program to print the login prompt.
//
// 	Most of this file is not needed until later assignments.
//
// Usage: nachos -d <debugflags> -rs <random seed #>
//		-s -x <nachos file> -c <consoleIn> <consoleOut>
//		-f -cp <unix file> <nachos file>
//		-p <nachos file> -r <nachos file> -l -D -t
//              -n <network reliability> -m <machine id>
//              -o <other machine id>
//              -z
//
//    -d causes certain debugging messages to be printed (cf. utility.h)
//    -rs causes Yield to occur at random (but repeatable) spots
//    -z prints the copyright message
//
//  USER_PROGRAM
//    -s causes user programs to be executed in single-step mode
//    -x runs a user program
//    -c tests the console
//
//  FILESYS
//    -f causes the physical disk to be formatted
//    -cp copies a file from UNIX to Nachos
//    -p prints a Nachos file to stdout
//    -r removes a Nachos file from the file system
//    -l lists the contents of the Nachos directory
//    -D prints the contents of the entire file system 
//    -t tests the performance of the Nachos file system
//
//  NETWORK
//    -n sets the network reliability
//    -m sets this machine's host id (needed for the network)
//    -o runs a simple test of the Nachos network software
//
//  NOTE -- flags are ignored until the relevant assignment.
//  Some of the flags are interpreted here; some in system.cc.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#define MAIN
#include "copyright.h"
#undef MAIN

#include "utility.h"
#include "system.h"

//STARTS HERE
#ifndef SCHED_POLICY
#define SCHED_POLICY
#define DEFAULT 1
#define SJF 2
#define RR1 3
#define RR2 4
#define RR3 5
#define RRBEST 6
#define UNIX1 7
#define UNIX2 8
#define UNIX3 9
#define UNIXBEST 10 
#endif
//ENDS HERE
// External functions used by this file


extern void ThreadTest(void), Copy(char *unixFile, char *nachosFile);
extern void Print(char *file), PerformanceTest(void);
extern void StartProcess(char *file), ConsoleTest(char *in, char *out);
extern void MailTest(int networkID);

//STARTS HERE

static void
TimerInterruptHandler(int dummy)
{
    int pol = scheduler->GetSchedPolicy(); 
	    TimeSortedWaitQueue *ptr;
	    if (interrupt->getStatus() != IdleMode) {
		// Check the head of the sleep queue
		while ((sleepQueueHead != NULL) && (sleepQueueHead->GetWhen() <= (unsigned)stats->totalTicks)) {
		   sleepQueueHead->GetThread()->Schedule();
		   ptr = sleepQueueHead;
		   sleepQueueHead = sleepQueueHead->GetNext();
		   delete ptr;
		}
		
	   switch(pol)
	   {
		   case DEFAULT:
		   case SJF:
		   {
			   break;
	           }
		   case UNIX1: case UNIX2: case UNIX3: case UNIXBEST:
		   case RR1: case RR2: case RR3: case RRBEST:
		   {
			   if(stats->totalTicks - currentThread->burstStart >= stats->TimerTicks)
			   {
				interrupt->YieldOnReturn();
			   }
			   break;
		   }
		   default:
		   {   
			interrupt->YieldOnReturn();
			break;
		   }
	   }
		//printf("[%d] Timer interrupt.\n", stats->totalTicks);
    }
}

Thread *t;

void
ThreadStartFunction (int dummy)
{
   t->Startup();
   machine->Run();
}

void ReadAndMakeThreads(char * list)
{
	FILE * fp;
	fp = fopen(list, "r");
	ASSERT( fp != NULL);
	char * fileLine; 	
	size_t len = 0;
	int priority;
	
	int schedPol;
	char fileName[40][1000];	//1000 size limit, 40 processes limit
	getline(&fileLine, &len, fp);
	sscanf(fileLine, "%d" , &schedPol); //scheduling policy as input
	printf("Scheduling policy: %d \n", schedPol);
	
	Thread *mainThread = currentThread;
	
	OpenFile *executable;
    	AddrSpace *space;
 
	scheduler->SetSchedPolicy(schedPol);
	switch(schedPol)
	{
		case RR1:
		{
			stats->TimerTicks = 33;
			break;
		}
		case RR2:
		{
			stats->TimerTicks = 66;
			break;
		}
		case RR3:
		{
			stats->TimerTicks = 98;
			break;
		}
		case RRBEST:
		{
			stats->TimerTicks = 20;
			break;
		}
		case UNIX1:
		{
			stats->TimerTicks = 33;
			break;
		}	
		case UNIX2:
		{
			stats->TimerTicks = 66;
			break;
		}	
		case UNIX3:
		{
			stats->TimerTicks = 98;
			break;
		}	
		case UNIXBEST:
		{
			stats->TimerTicks = 20;
			break;
		}	
		default:
		{
			break;
		}
	}

	timer = new Timer(TimerInterruptHandler, 0, false);
	
	currentThread->SaveUserState();
	int lineNo = 0;	
	while(getline(&fileLine, &len, fp) != -1)
	{
		priority = 100;
		sscanf(fileLine, "%s%d", fileName[lineNo], &priority);
		//create threads
		t = new Thread(fileName[lineNo]);
			
		// Based on userprog/progtest.cc StartProcess	
        	executable = fileSystem->Open(fileName[lineNo]);
        	if(executable == NULL)
        	{
        		printf("Unable to open file %s\n", fileName[lineNo]);
        	}
        	space = new AddrSpace(executable);
		delete executable;
        	
        	space->InitRegisters();		// set the initial register values
	
        	t->space = space;

		//Only for unix scheduler
		t->basePriority += priority;
		t->dynamicPriority = t->basePriority;
		//unix scheduler priority assignment ends
	
		t->StackAllocate(ThreadStartFunction, 0);
		t->SaveUserState();
		t->Schedule();	
		stats->noOfProcess++;
		lineNo++;
	}	
	//syscall_Exit code
        mainThread->RestoreUserState();
        unsigned int i;
        int exitcode = 0;
        printf("[pid %d]: Exit called. Code: %d\n", mainThread->GetPID(), exitcode);
        exitThreadArray[mainThread->GetPID()] = true;

        for (i=0; i<thread_index; i++) {
           if (!exitThreadArray[i]) break;
        }
        mainThread->Exit(i==thread_index, exitcode);
}
//ENDS HERE

//----------------------------------------------------------------------
// main
// 	Bootstrap the operating system kernel.  
//	
//	Check command line arguments
//	Initialize data structures
//	(optionally) Call test procedure
//
//	"argc" is the number of command line arguments (including the name
//		of the command) -- ex: "nachos -d +" -> argc = 3 
//	"argv" is an array of strings, one for each command line argument
//		ex: "nachos -d +" -> argv = {"nachos", "-d", "+"}
//----------------------------------------------------------------------

int
main(int argc, char **argv)
{
    int argCount;			// the number of arguments 
					// for a particular command

    DEBUG('t', "Entering main");
    (void) Initialize(argc, argv);
    
#ifdef THREADS
    ThreadTest();
#endif

    for (argc--, argv++; argc > 0; argc -= argCount, argv += argCount) {
	argCount = 1;
        if (!strcmp(*argv, "-z"))               // print copyright
            printf (copyright);
//STARTS HERE	    
        if(!strcmp(*argv, "-F")){
            ASSERT(argc > 1);
            ReadAndMakeThreads(*(argv + 1));	//read file and make threads 
            argCount = 2;			//-F filename
        }
//ENDS HERE		
#ifdef USER_PROGRAM
        if (!strcmp(*argv, "-x")) {        	// run a user program
	    ASSERT(argc > 1);
            StartProcess(*(argv + 1));
            argCount = 2;
        } else if (!strcmp(*argv, "-c")) {      // test the console
	    if (argc == 1)
	        ConsoleTest(NULL, NULL);
	    else {
		ASSERT(argc > 2);
	        ConsoleTest(*(argv + 1), *(argv + 2));
	        argCount = 3;
	    }
	    interrupt->Halt();		// once we start the console, then 
					// Nachos will loop forever waiting 
					// for console input
	}
#endif // USER_PROGRAM
#ifdef FILESYS
	if (!strcmp(*argv, "-cp")) { 		// copy from UNIX to Nachos
	    ASSERT(argc > 2);
	    Copy(*(argv + 1), *(argv + 2));
	    argCount = 3;
	} else if (!strcmp(*argv, "-p")) {	// print a Nachos file
	    ASSERT(argc > 1);
	    Print(*(argv + 1));
	    argCount = 2;
	} else if (!strcmp(*argv, "-r")) {	// remove Nachos file
	    ASSERT(argc > 1);
	    fileSystem->Remove(*(argv + 1));
	    argCount = 2;
	} else if (!strcmp(*argv, "-l")) {	// list Nachos directory
            fileSystem->List();
	} else if (!strcmp(*argv, "-D")) {	// print entire filesystem
            fileSystem->Print();
	} else if (!strcmp(*argv, "-t")) {	// performance test
            PerformanceTest();
	}
#endif // FILESYS
#ifdef NETWORK
        if (!strcmp(*argv, "-o")) {
	    ASSERT(argc > 1);
            Delay(2); 				// delay for 2 seconds
						// to give the user time to 
						// start up another nachos
            MailTest(atoi(*(argv + 1)));
            argCount = 2;
        }
#endif // NETWORK
    }

    currentThread->Finish();	// NOTE: if the procedure "main" 
				// returns, then the program "nachos"
				// will exit (as any other normal program
				// would).  But there may be other
				// threads on the ready list.  We switch
				// to those threads by saying that the
				// "main" thread is finished, preventing
				// it from returning.
    return(0);			// Not reached...
}
