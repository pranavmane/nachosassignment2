// scheduler.h 
//	Data structures for the thread dispatcher and scheduler.
//	Primarily, the list of threads that are ready to run.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef SCHEDULER_H
#define SCHEDULER_H

//STARTS HERE
#ifndef SCHED_POLICY
#define SCHED_POLICY
#define DEFAULT 1
#define SJF 2
#define RR1 3
#define RR2 4
#define RR3 5
#define RRBEST 6
#define UNIX1 7
#define UNIX2 8
#define UNIX3 9
#define UNIXBEST 10 
#endif
//ENDS HERE

#include "copyright.h"
#include "list.h"
#include "thread.h"

// The following class defines the scheduler/dispatcher abstraction -- 
// the data structures and operations needed to keep track of which 
// thread is running, and which threads are ready but not running.

class Scheduler {
  public:
    Scheduler();			// Initialize list of ready threads 
    ~Scheduler();			// De-allocate ready list

    void ReadyToRun(Thread* thread);	// Thread can be dispatched.
    Thread* FindNextToRun();		// Dequeue first thread on the ready 
					// list, if any, and return thread.
    void Run(Thread* nextThread);	// Cause nextThread to start running
    void Print();			// Print contents of ready list
    
    void Tail();                        // Used by fork()
  //STARTS HERE  
    void SetSchedPolicy(int policy);
    int GetSchedPolicy();
    void UpdateAllPriorities();
  //ENDS HERE
  private:
    List *readyList;  		// queue of threads that are ready to run,
				// but not running
    int schedPolicy;
};

#endif // SCHEDULER_H
