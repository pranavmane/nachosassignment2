// stats.h 
//	Routines for managing statistics about Nachos performance.
//
// DO NOT CHANGE -- these stats are maintained by the machine emulation.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "stats.h"

//----------------------------------------------------------------------
// Statistics::Statistics
// 	Initialize performance metrics to zero, at system startup.
//----------------------------------------------------------------------

Statistics::Statistics()
{
    totalTicks = idleTicks = systemTicks = userTicks = 0;
    numDiskReads = numDiskWrites = 0;
    numConsoleCharsRead = numConsoleCharsWritten = 0;
    numPageFaults = numPacketsSent = numPacketsRecvd = 0;
    //STARTS HERE
    totalWait=0;
    noOfProcess=0; //Main is already a process
    TimerTicks = 100;	//Default TimerTicks
    BurstTicks=0;
    maxBurstTicks = 0;
    minBurstTicks = 10000;
    numBurstTicks = 0;
    maxCompletionTime = 0;
    minCompletionTime = 10000;
    sumCompletionTime = 0;
    sumSqCompletionTime = 0;
    absEstError = 0;    //To estimate the error in CPU burst estimation
    sjfCpuBurst = 0;    //Sum of all CPU bursts except that of main.cc
    //ENDS HERE
}

//----------------------------------------------------------------------
// Statistics::Print
// 	Print performance metrics, when we've finished everything
//	at system shutdown.
//----------------------------------------------------------------------

void
Statistics::Print()
{
    //STARTS HERE
    if(sjfCpuBurst)
	    printf("Overall estimation error: %f\n",(double)(absEstError)/(sjfCpuBurst*1.0));
    //printf("SJF CPU Burst: %d\n",sjfCpuBurst);
    printf("Total CPU busy time: %d\n",totalTicks-idleTicks);	
    printf("Total execution time: %d\n",totalTicks);    
    printf("CPU utilization: %d, Percentage %f %%\n",BurstTicks,(double)(BurstTicks*100)/(1.0*totalTicks)); // Total CPU burst
    printf("CPU burst length: maximum %d, minimum %d, average %f\n",maxBurstTicks,minBurstTicks,((double)BurstTicks/(double)(1.0*numBurstTicks)));
    printf("Number of non-zero CPU bursts observed: %d\n",numBurstTicks);
    printf("Average Waiting Time: %f\n",((double)totalWait)/(double)(1.0*(noOfProcess+1)));
    printf("Thread completion times: maximum %d, minimum %d, average %f, variance %f\n",maxCompletionTime,minCompletionTime,sumCompletionTime/noOfProcess,sumSqCompletionTime/noOfProcess-(sumCompletionTime/noOfProcess)*(sumCompletionTime/noOfProcess));
    //ENDS HERE
    printf("Ticks: total %d, idle %d, system %d, user %d\n", totalTicks, 
	idleTicks, systemTicks, userTicks);
    printf("Disk I/O: reads %d, writes %d\n", numDiskReads, numDiskWrites);
    printf("Console I/O: reads %d, writes %d\n", numConsoleCharsRead, 
	numConsoleCharsWritten);
    printf("Paging: faults %d\n", numPageFaults);
    printf("Network I/O: packets received %d, sent %d\n", numPacketsRecvd, 
	numPacketsSent);
}
